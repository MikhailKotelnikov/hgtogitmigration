(function() {
	importClass(java.io.File);
	var dirName = antProject.getProperty("workdir");
	var scriptDir = antProject.getProperty("scriptdir");
	var targets = antProject.getTargets();
	var dir = new java.io.File(dirName);
	var list = dir.listFiles();

	// Split all projects
	var serverStart = targets.get("git.server.start");
	var serverStop = targets.get("git.server.stop");
	serverStart.execute();
	try {
		cloneHgProjects([
				"https://mikhail.kotelnikov@code.google.com/p/webreformatter.poms/",
				"https://mikhail.kotelnikov@code.google.com/p/webreformatter.libs/",
				"https://mikhail.kotelnikov@code.google.com/p/webreformatter.commons/",
				"https://mikhail.kotelnikov@code.google.com/p/webreformatter/",
				"https://mikhail.kotelnikov@code.google.com/p/webreformatter.opensocial/" ]);
		checkoutHgProjectBranch("webreformatter.opensocial", "opensocial-v2");
		convertHgToGit([ "webreformatter.poms", "webreformatter.libs",
				"webreformatter.commons", "webreformatter.opensocial" ])

		splitGitProject("webreformatter.poms", ".", [
				"org.webreformatter.poms.bundle",
				"org.webreformatter.poms.gwt",
				"org.webreformatter.poms.gwt.app" ]);

		splitGitProject("webreformatter.libs", "./projects", [
				"com.jcraft.jsch", "org.apache.commons.httpclient",
				"org.apache.lucene", "org.freemarker", "org.htmlcleaner",
				"org.jivesoftware.smack", "org.stringtemplate",
				"org.yaml.snakeyaml" ]);

		splitGitProject("webreformatter.commons", "./projects", [
				"org.webreformatter.commons.adapters",
				"org.webreformatter.commons.events",
				"org.webreformatter.commons.fs",
				"org.webreformatter.commons.geo",
				"org.webreformatter.commons.iterator",
				"org.webreformatter.commons.json",
				"org.webreformatter.commons.json.rpc.servlet",
				"org.webreformatter.commons.osgi",
				"org.webreformatter.commons.rpc",
				"org.webreformatter.commons.strings",
				"org.webreformatter.commons.uri",
				"org.webreformatter.commons.utils",
				"org.webreformatter.commons.xml" ]);

		splitGitProject("webreformatter.opensocial", "./libs", [
				"org.webreformatter.libs.feedtools",
				"org.webreformatter.libs.oauth",
				"org.webreformatter.libs.scribe" ]);
		splitGitProject("webreformatter.opensocial", "./projects", [
				"org.webreformatter.opensocial.api",
				"org.webreformatter.opensocial.base",
				"org.webreformatter.opensocial.facebook",
				"org.webreformatter.opensocial.feeds",
				"org.webreformatter.opensocial.mail",
				"org.webreformatter.opensocial.oauth",
				"org.webreformatter.opensocial.store",
				"org.webreformatter.opensocial.twitter",
				"org.webreformatter.sandbox.tests" ]);

	} finally {
		serverStop.execute();
	}

	function splitGitProject(project, dir, modules) {
		var gitsplit = targets.get("git.split");
		for ( var i = 0; i < modules.length; i++) {
			antProject.setProperty("git.split-project", project);
			antProject.setProperty("git.split-dir", project);
			antProject.setProperty("git.split-module", modules[i]);
			gitsplit.execute();
		}
	}

	function convertHgToGit(projects) {
		var hgtogit = targets.get("hg.to.git");
		for ( var i = 0; i < projects.length; i++) {
			antProject.setProperty("hg.to.git-project", projects[i]);
			hgtogit.execute();
		}
	}

	// Checkout a specific branch
	function checkoutHgProjectBranch(project, branch) {
		var hgcheckout = targets.get("hg.checkout");
		antProject.setProperty("hg.checkout-project", project);
		antProject.setProperty("hg.checkout-branch", branch);
		hgcheckout.execute();
	}
	// Clone all Hg projects
	function cloneHgProjects(hgprojects) {
		var hgclone = targets.get("hg.clone");
		for ( var i = 0; i < hgprojects.length; i++) {
			antProject.setProperty("hg.clone-url", hgprojects[i]);
			hgclone.execute();
		}
	}
})();