#!/bin/bash

# TODO: Check old versions :
# >grep -R "version>1.1.5" org.webreformatter.*

dir=`pwd`
cd `dirname "$0"`
cd ../scripts
home=`pwd`

newVersion=1.2.0
commitMsg="Migration to Git"
commitUsr="mikhail.kotelnikov"

function example() {
    array=($@)
    len=${#array[*]}
    i=1
    while [ $i -lt $len ]; do
        echo "$i: ${array[$i]}"
        let i++
    done
}

function split() {
    array=($@)
    module=${array[0]}
    dir=${array[1]}
    len=${#array[*]}
    i=2
    while [ $i -lt $len ]; do
        submodule=${array[$i]}
        ${home}/split.sh ${module} ${dir} ${submodule}
        let i++
    done
}

function mvn_install() {
    array=($@)
    len=${#array[*]}
    project=${array[0]}
    subdir=${array[1]}
    echo "========================================================"
    echo "Maven install: PROJECT='${project}' SUBDIR='${subdir}'"
    i=2
    while [ $i -lt $len ]; do
        module=${array[$i]}
        cd ${workdir}/${project}/${subdir}/${module}
        echo ""
        echo "Install Maven artefacts : " `pwd`
        mvn clean install
        let i++
    done
    cd ${workdir}
}

function update_versions() {
    array=($@)
    len=${#array[*]}
    project=${array[0]}
    subdir=${array[1]}
    echo "========================================================"
    echo "Update versions: PROJECT='${project}' SUBDIR='${subdir}'"
    i=2
    while [ $i -lt $len ]; do
        module=${array[$i]}
        cd ${workdir}/${project}/${subdir}/${module}
        echo ""
        echo "Migrate : " `pwd`
        mvn versions:set versions:update-parent -DnewVersion=${newVersion}
        let i++
    done
    cd ${workdir}/${project}
    hg commit -X *.xml.versionsBackup -m "${commitMsg}" -u "${commitUsr}" 
    cd ${workdir}
}

workdir=${dir}
if [ ! -d ${workdir} ]; then
  mkdir ${workdir}
fi
cd ${workdir}

# Start a local git deamon
${home}/deamon-start.sh


function hg2git() {
    cd ${workdir}
    ${home}/export-repo.sh "$1"
    cd "${workdir}/$1"
    git branch "version-${newVersion}"
    cd ${workdir}
}

############################################ 
### webreformatter.poms
############################################
function migrate_poms() {
    # Clone all projects in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.poms/
    
    # Migrate from Hg to Git
    update_versions webreformatter.poms . org.webreformatter.poms.bundle org.webreformatter.poms.gwt org.webreformatter.poms.gwt.app
    
    # Install new version of maven artefacts
    mvn_install  webreformatter.poms . org.webreformatter.poms.bundle org.webreformatter.poms.gwt org.webreformatter.poms.gwt.app
    
    hg2git webreformatter.poms
    
    # Split projects
    split webreformatter.poms . org.webreformatter.poms.bundle org.webreformatter.poms.gwt org.webreformatter.poms.gwt.app
}
 
############################################ 
### webreformatter.libs
############################################
function migrate_libs() { 
    # The project in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.libs/

    # Migrate from Hg to Git
    hg2git webreformatter.libs
    
    # Split projects
    split webreformatter.libs ./projects com.jcraft.jsch org.apache.commons.httpclient org.apache.lucene org.freemarker org.htmlcleaner org.jivesoftware.smack org.stringtemplate org.yaml.snakeyaml
    mv ${workdir}/com.jcraft.jsch ${workdir}/org.webreformatter.libs.jsch 
    mv ${workdir}/org.apache.commons.httpclient ${workdir}/org.webreformatter.libs.httpclient
    mv ${workdir}/org.apache.lucene ${workdir}/org.webreformatter.libs.lucene
    mv ${workdir}/org.freemarker ${workdir}/org.webreformatter.libs.freemarker
    mv ${workdir}/org.htmlcleaner ${workdir}/org.webreformatter.libs.htmlcleaner
    mv ${workdir}/org.jivesoftware.smack ${workdir}/org.webreformatter.libs.smack
    mv ${workdir}/org.stringtemplate ${workdir}/org.webreformatter.libs.stringtemplate
    mv ${workdir}/org.yaml.snakeyaml ${workdir}/org.webreformatter.libs.snakeyaml
}

############################################
### webreformatter.commons
############################################
function migrate_commons() {
    # Clone all projects in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.commons/

    # Update the version number 
    update_versions webreformatter.commons . projects
    update_versions webreformatter.commons projects org.webreformatter.commons.geo

    # Migrate from Hg to Git
    hg2git webreformatter.commons

    # Build an install the latest version
    mvn_install webreformatter.commons . projects
    
    # Split projects
    split webreformatter.commons projects org.webreformatter.commons.adapters org.webreformatter.commons.events org.webreformatter.commons.fs org.webreformatter.commons.geo org.webreformatter.commons.iterator org.webreformatter.commons.json org.webreformatter.commons.json.rpc.servlet org.webreformatter.commons.osgi org.webreformatter.commons.rpc org.webreformatter.commons.strings org.webreformatter.commons.uri org.webreformatter.commons.utils org.webreformatter.commons.xml
}

############################################ 
### webreformatter - ?
############################################
# The project in the local folder 
# Migrate from Hg to Git
# Split projects

############################################ 
### webreformatter.opensocial
############################################
function migrate_opensocial() {
    # The project in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.opensocial/
    
    # Update the version number 
    update_versions webreformatter.opensocial . libs
    update_versions webreformatter.opensocial . projects
    update_versions webreformatter.opensocial projects org.webreformatter.opensocial.api org.webreformatter.opensocial.base org.webreformatter.opensocial.facebook org.webreformatter.opensocial.feeds org.webreformatter.opensocial.mail org.webreformatter.opensocial.oauth org.webreformatter.opensocial.store org.webreformatter.opensocial.twitter org.webreformatter.sandbox.tests

    # Migrate from Hg to Git
    hg2git webreformatter.opensocial
    
    # Split projects
    split webreformatter.opensocial libs org.webreformatter.libs.feedtools org.webreformatter.libs.oauth org.webreformatter.libs.scribe
    split webreformatter.opensocial projects org.webreformatter.opensocial.api org.webreformatter.opensocial.base org.webreformatter.opensocial.facebook org.webreformatter.opensocial.feeds org.webreformatter.opensocial.mail org.webreformatter.opensocial.oauth org.webreformatter.opensocial.store org.webreformatter.opensocial.twitter org.webreformatter.sandbox.tests
}


############################################ 
### webreformatter.templates
############################################ 
function migrate_templates() { 
    # The project in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.templates/
    
    # Update the version number 
    update_versions webreformatter.templates . projects
    update_versions webreformatter.templates projects org.webreformatter.commons.templates org.webreformatter.commons.templates.freemarker org.webreformatter.commons.templates.stringtemplate org.webreformatter.commons.templates.velocity

    # Migrate from Hg to Git
    hg2git webreformatter.templates
    
    # Split projects
    split webreformatter.templates projects org.webreformatter.commons.templates org.webreformatter.commons.templates.freemarker org.webreformatter.commons.templates.stringtemplate org.webreformatter.commons.templates.velocity
    mv ${workdir}/org.webreformatter.commons.templates ${workdir}/org.webreformatter.commons.templates.api
}

############################################ 
### webreformatter.sync
############################################
function migrate_sync() { 
    # The project in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.sync/

    update_versions webreformatter.sync . projects
    update_versions webreformatter.sync projects org.webreformatter.search org.webreformatter.sync.libs org.webreformatter.sync.main

    # Migrate from Hg to Git
    hg2git webreformatter.sync
    
    # Split projects
    split webreformatter.sync projects org.webreformatter.search org.webreformatter.sync.libs org.webreformatter.sync.main 
    mv ${workdir}/org.webreformatter.search ${workdir}/org.webreformatter.commons.search
    mv ${workdir}/org.webreformatter.sync.libs ${workdir}/org.webreformatter.sync.poms
}

############################################ 
### webreformatter.scrapper
############################################
function migrate_scrapper() {
    # The project in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.scrapper/
    
    update_versions webreformatter.scrapper . projects
    update_versions webreformatter.scrapper projects  org.webreformatter.scrapper.app org.webreformatter.scrapper.example org.webreformatter.scrapper.pageset org.webreformatter.scrapper.protocol org.webreformatter.scrapper.resources org.webreformatter.scrapper.transformer org.webreformatter.vaadin

    # Migrate from Hg to Git
    hg2git webreformatter.scrapper
    
    # Split projects
    split webreformatter.scrapper projects  org.webreformatter.scrapper.app org.webreformatter.scrapper.example org.webreformatter.scrapper.pageset org.webreformatter.scrapper.protocol org.webreformatter.scrapper.resources org.webreformatter.scrapper.transformer org.webreformatter.vaadin 
    mv ${workdir}/org.webreformatter.vaadin ${workdir}/org.webreformatter.commons.vaadin
    
    # !!!!!!!!!!!!!!!!!!!
    # FIXME: add "webreformatter.scrapper/workdir" migration
}

############################################ 
### webreformatter.ebook
############################################
function migrate_ebook() {
    # The project in the local folder 
    hg clone https://mikhail.kotelnikov@code.google.com/p/webreformatter.ebook/
    
    update_versions webreformatter.ebook . projects
    update_versions webreformatter.ebook projects org.webreformatter.ebook org.webreformatter.ebook.generator org.webreformatter.ebook.reader

    # Migrate from Hg to Git
    hg2git webreformatter.ebook
    
    # Split projects
    split webreformatter.ebook projects org.webreformatter.ebook org.webreformatter.ebook.generator org.webreformatter.ebook.reader  
    mv ${workdir}/org.webreformatter.ebook ${workdir}/org.webreformatter.ebook.api
    
    # !!!!!!!!!!!!!!!!!!!
    # FIXME: add "webreformatter.ebook/workdir" migration
}

migrate_poms
migrate_libs
migrate_commons
migrate_opensocial
migrate_templates
migrate_sync 
migrate_scrapper
migrate_ebook

# Stop a local git deamon
${home}/deamon-stop.sh

