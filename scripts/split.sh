#!/bin/bash

# This Git splitting solution is based on the following StackOverflow discussion:
# * http://stackoverflow.com/a/1591174 

# Project to split:
project=$1
# A subdirectory in the project. Could be ".": 
subdir=$2
# Name of the module to extract: 
module=$3

function help {
   echo "Usage:"
   echo ">split.sh <PROJECT_TO_SPLIT> <SUBDIR> <MODULE>"
}
if [ "${project}" == "" ]; then
  help
  exit -1
fi
if [ "${subdir}" == "" ]; then
  help
  exit -1
fi 
if [ "${module}" == "" ]; then
  help
  exit -1
fi 

git clone --no-hardlinks ${project} ${module}
cd ${module}
git filter-branch --subdirectory-filter ${subdir}/${module}
git remote rm origin
git update-ref -d refs/original/refs/heads/master
git reflog expire --expire=now --all
git repack -ad
