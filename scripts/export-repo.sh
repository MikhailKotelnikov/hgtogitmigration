#!/bin/bash
repo=$1
dir=`pwd`
if [ "${repo}" == "" ]; then
  echo "Usage:"
  echo ">export-repo.sh <HG_PROJECT_NAME>"
  exit -1
fi
if [ ! -d "${dir}/${repo}" ]; then
  echo "Project ${dir}/${repo} was not found."
  exit -1
fi

cd "${dir}/git-repository"
rm -fr "./${repo}.git"
mkdir "${repo}.git"
cd "${repo}.git"
git --bare init
cd "${dir}/${repo}"
hg bookmark -r default master
hg push git://localhost/${repo}.git
hgRevisionId=`hg id --debug -i`
cd "${dir}"

hgrepodir=${dir}/hg
if [ ! -d ${hgrepodir} ]; then
  mkdir ${hgrepodir}
fi
mv "${repo}" "${hgrepodir}/${repo}"
git clone git://localhost/${repo}.git
cd "${repo}"
git tag -a "hg-import-${hgRevisionId}" -m "Import from Hg (version ${hgRevisionId})" 


