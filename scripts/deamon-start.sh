#!/bin/bash
dir=`pwd`
pidfile="${dir}/process.pid"
if [ ! -d ${dir}/git-repository ]; then
  mkdir ${dir}/git-repository
fi
touch ${dir}/git-repository/git-daemon-export-ok
git daemon --base-path=${dir}/git-repository --export-all --enable=upload-pack --enable=upload-archive --enable=receive-pack --pid-file=${pidfile} &


