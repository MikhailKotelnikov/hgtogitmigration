#/bin/bash
dir=`pwd`
if [ -f ${dir}/process.pid ]; then
  pid=`cat ${dir}/process.pid`
  kill ${pid}
  rm -f ${dir}/process.pid
else
  echo "Can not kill the Git deamon: ${dir}/process.pid file not found." 
fi

