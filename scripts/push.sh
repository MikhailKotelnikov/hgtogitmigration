#!/bin/bash
repo=$1
dir=`pwd`
if [ "${repo}" == "" ]; then
  repo=`echo ${PWD##*/}`
  cd ..
  dir=`pwd`
fi
if [ ! -d "${dir}/${repo}" ]; then
  echo "Project ${dir}/${repo} was not found."
  exit -1
fi

cd "${dir}/git-repository"
if [ ! -d "${repo}.git" ]; then
   mkdir "${repo}.git"
   cd "${repo}.git"
   git --bare init
   cd "${dir}/${repo}"
   git remote rm origin 
   git remote add origin git://localhost/${repo}.git
fi

function parse_git_branch {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
cd "${dir}/${repo}"
branch=`parse_git_branch`
git push git://localhost/${repo}.git ${branch} 



